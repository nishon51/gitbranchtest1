package tandukar.nishon.gitbranchtest1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showToast();

    }

    private void showToast(){
        Toast.makeText(MainActivity.this, "Hello ! I am a Toasty Toast", Toast.LENGTH_SHORT).show();
    }
}
